<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);




$api->version('v1', function (Router $api) {




    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->get('test/email','App\\Api\\V1\\Controllers\\SignUpController@test');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
        $api->post('validate/otp','App\\Api\\V1\\Controllers\\SignUpController@validateOTP');
        $api->post('resend/otp','App\\Api\\V1\\Controllers\\SignUpController@resendOTP');

        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->get('me', 'App\\Api\\V1\\Controllers\\UserController@me');
    });


    //admin panel routes
    $api->get('get/users','App\Http\Controllers\AdminController@getUsers');
    $api->get('get/emergency','App\Http\Controllers\AdminController@getEmergencies');
    $api->post('send/email','App\Http\Controllers\AdminController@sendEmailAdmin');
    $api->post('block/user','App\Http\Controllers\AdminController@blockUser');
    $api->post('unblock/user','App\Http\Controllers\AdminController@unBlockUser');



    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->post('add/emails','App\Http\Controllers\AppController@addEmailAddresses');
        $api->post('add/phones','App\Http\Controllers\AppController@addPhoneNumbers');
        $api->post('add/emergency','App\\Api\\V1\\Controllers\\UserController@createEmergency');


        $api->get('test/emergency','App\Http\Controllers\EmergencyController@testEmergency');



        $api->get('get/phones','App\\Api\\V1\\Controllers\\UserController@getUserPhoneNumbers');
        $api->get('get/emails','App\\Api\\V1\\Controllers\\UserController@getUserEmailAddresses');
        $api->post('add/contacts','App\\Api\\V1\\Controllers\\UserController@addContacts');


        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });

    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });


});
