<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailAddress extends Model
{
    //
    protected $table='email_address';


    protected $fillable=['user_id','email_address'];

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
