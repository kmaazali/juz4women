<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneNumber extends Model
{
    //
    protected $table='phone_numbers';

    protected $fillable=['user_id','phone_number'];

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
