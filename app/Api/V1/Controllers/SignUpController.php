<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use PHPMailer;
class SignUpController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {
        if ($request->has('phone_number')&&$request['phone_number']!=''&&$request->has('dob')&&$request['dob']!=''&&$request->has('address')&&$request['address']!=''&&$request->has('country')&&$request['country']!=''&&$request->has('state')&&$request['state']!=''&&$request->has('city')&&$request['city']!=''&&$request->has('role_id')&&$request['role_id']!='') {
            $user = new User($request->all());
            $this->mailObject($request['email']);
            $token = $JWTAuth->fromUser($user);

            $mobile_number=$request['phone_number'];

            $curl = curl_init();
//
            curl_setopt_array($curl, array(
            CURLOPT_URL => "http://control.msg91.com/api/sendotp.php?otp_length=4&authkey=230460A32Zvsrd0awk5b698b9c&message=Your%20Verification%20Code%20Is%20%23%23OTP%23%23&sender=JZ4WMN&mobile=".$mobile_number."",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);


            if (!$user->save()) {
                throw new HttpException(500);
            }

            if (!Config::get('boilerplate.sign_up.release_token')) {
                return response()->json([
                    'status' => 'ok',
                    'user' => $user,
                    'message'=>'User Signed Up Successfully'
                ], 201);
            }






            return response()->json([
                'status' => 'ok',
                'token' => $token,
                'user' => $user,
                'message'=>'User Signed Up Successfully'
            ], 201);
        }
        else {
            return response()->json(['success'=>false,'message'=>'Please Fill In All The Required Fields'],406);
        }
    }


    public function resendOTP(Request $request)
    {

        if ($request->has('mobile')) {
            $mobile=$request['mobile'];
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://control.msg91.com/api/retryotp.php?authkey=230460A32Zvsrd0awk5b698b9c&mobile=".$mobile."",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                return response()->json(['success'=>true,'message'=>'Your OTP has been successfully send'],200);
            }
        }
    }

    public function validateOTP(Request $request)
    {

        if ($request->has('otp') && $request->has('mobile')) {
            $curl = curl_init();
            $otp=$request['otp'];
            $mobile=$request['mobile'];
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://control.msg91.com/api/verifyRequestOTP.php?authkey=230460A32Zvsrd0awk5b698b9c&mobile=$mobile&otp=$otp",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            //$user_id=auth()->user()->id;
            User::where('phone_number',$mobile)->update(['is_verified'=>1]);
            return response()->json(['success'=>true,'message'=>'OTP Successfully Validated'],200);

        }
        else{
            return response()->json(['success'=>false,'message'=>'Kindly Provide A Valid Mobile Number & OTP'],406);
        }
    }



    public function test(){
        return $this->mailObject();

    }
    public function generateRandomString($length = 4) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function mailObject($reciever){
        //Load Composer's autoloader


        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'juz4womenapp@gmail.com';                 // SMTP username
            $mail->Password = '4Inova.com';                           // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('juz4womenapp@gmail.com', 'Juz 4 Women');
            $mail->addAddress($reciever);     // Add a recipient
            //$mail->addAddress('ellen@example.com');               // Name is optional
            //$mail->addReplyTo('info@example.com', 'Information');
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');

            //Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Welcome';
            $mail->Body    = 'Thank you for signing up on <b>Juz4Women App</b>';
            $mail->AltBody = 'Thank you for signing up on <b>Juz4Women App</b>';

            $mail->send();
           // echo 'Message has been sent';
        } catch (Exception $e) {
            //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }

    }

}

