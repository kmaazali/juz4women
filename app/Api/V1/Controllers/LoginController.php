<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\User;

class LoginController extends Controller
{
    /**
     * Log the user in
     *
     * @param LoginRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {
        if ($request->has('email') && $request['email'] != '' && $request->has('player_id') && $request['player_id'] != '') {
            $credentials = $request->only(['email', 'password']);
            $user = User::where('email', $request['email'])->with('role')->first();
            if ($user) {
                $user->update(['player_id'=>$request['player_id']]);
                $user->save();
                try {
                    $token = Auth::guard()->attempt($credentials);

                    if (!$token) {
                        throw new AccessDeniedHttpException();
                    }

                } catch (JWTException $e) {
                    throw new HttpException(500);
                }

                return response()
                    ->json([
                        'status' => 'ok',
                        'token' => $token,
                        'user' => $user,
                        'expires_in' => Auth::guard()->factory()->getTTL() * 60
                    ]);
            }
            else{
                return response()->json(['success'=>false,'message'=>'Invalid Credentials'],406);
            }
        }
        else{
            return response()->json(['success'=>false,'message'=>'Kindly Enter Required Fields'],406);
        }
    }
}
