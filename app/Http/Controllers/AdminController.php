<?php

namespace App\Http\Controllers;
use App\User;
use PHPMailer;
use App\Emergency;
use App\PhoneNumber;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //

    public function mailObject($to,$subject,$message){
        //Load Composer's autoloader


        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'juz4womenapp@gmail.com';                 // SMTP username
            $mail->Password = '4Inova.com';                           // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('juz4womenapp@gmail.com', 'Juz 4 Women');
            $mail->addAddress($to);     // Add a recipient
            //$mail->addAddress('ellen@example.com');               // Name is optional
            //$mail->addReplyTo('info@example.com', 'Information');
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');

            //Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $message;
            $mail->AltBody = $message;

            $mail->send();
            return response()->json(['success'=>true,'message'=>'Mail Has Been Successfully Sent']);
        } catch (Exception $e) {
            return response()->json(['success'=>false,'error'=>$mail->ErrorInfo],406);

        }

    }

    public function sendEmailAdmin(Request $request){
        if($request->has('to')&&$request['to']!=''&&$request->has('subject')&&$request['subject']!=''&&$request->has('message')&&$request['message']!=''){
            return $this->mailObject($request['to'],$request['subject'],$request['message']);

        }
        else{
            return response()->json(['success'=>false,'message'=>'Please Enter The Required Fields'],406);
        }

    }

    public function getUsers(){
        $user=User::get();
        return response()->json(['success'=>true,'users'=>$user],200);
    }

    public function getEmergencies(){
        $emergencies=Emergency::with('user')->get();
        return response()->json(['success'=>true,'emergency'=>$emergencies],200);
    }

    public function blockUser(Request $request){

        User::where('id',$request['profile_id'])->update(['is_suspended' => 1]);
        return response()->json(['success'=>true,'message'=>'User Blocked Successfully'],200);
    }
    public function unBlockUser(Request $request){
        User::where('id',$request['profile_id'])->update(['is_suspended'=>0]);
        return response()->json(['success'=>true,'message'=>'User Blocked Successfully'],200);
    }
}
