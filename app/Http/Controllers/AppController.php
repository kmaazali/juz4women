<?php

namespace App\Http\Controllers;

use App\EmailAddress;
use App\User;
use App\PhoneNumber;
use Illuminate\Http\Request;

class AppController extends Controller
{
    //
    public function addEmailAddresses(Request $request){
        if($request->has('emails')){
            $user_id=auth()->user()->id;
            $user=User::where('id',$user_id)->first();
            $user->update(['emails_added'=>true]);
            $user->save();
            foreach ($request['emails'] as $email){
                EmailAddress::create([
                    'user_id'=>$user_id,
                    'email_address'=>$email
                ]);
            }
            return response()->json(['success'=>true,'message'=>'Emails Added Successfully'],200);
        }
        else{
            return response()->json(['success'=>false,'message'=>'Please Enter At Least 1 Email'],406);
        }

    }

    public function addPhoneNumbers(Request $request){
        if($request->has('phone_numbers')){
            $user_id=auth()->user()->id;
            $user=User::where('id',$user_id)->first();
            $user->update(['phones_added'=>true]);
            $user->save();
            foreach ($request['phone_numbers'] as $phone_number){
                PhoneNumber::create([
                    'user_id'=>$user_id,
                    'phone_number'=>$phone_number
                ]);
            }
            return response()->json(['success'=>true,'message'=>'Phone Numbers Added Successfully'],200);
        }
        else{
            return response()->json(['success'=>false,'message'=>'Please Enter At Least 1 Email'],406);
        }
    }
}
