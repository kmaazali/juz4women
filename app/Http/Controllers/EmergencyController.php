<?php

namespace App\Http\Controllers;


use App\User;
use App\EmailAddress;
use App\Emergency;
use App\PhoneNumber;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use Request;
use PHPMailer;

class EmergencyController extends Controller
{
    //



    public function mailObject($to,$from){
        //Load Composer's autoloader


        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'juz4womenapp@gmail.com';                 // SMTP username
            $mail->Password = '4Inova.com';                           // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('juz4womenapp@gmail.com', 'Juz 4 Women');
            $mail->addAddress($to);     // Add a recipient
            //$mail->addAddress('ellen@example.com');               // Name is optional
            //$mail->addReplyTo('info@example.com', 'Information');
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');

            //Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = "Emergency Test";
            $mail->Body    = "<h1>A Test Emergency Has Been Raised From The User ".$from." To Your Email Address.</h1>";
            $mail->AltBody = "<h1>A Test Emergency Has Been Raised From The User ".$from." To Your Email Address.</h1>";

            $mail->send();
            return response()->json(['success'=>true,'message'=>'Mail Has Been Successfully Sent']);
        } catch (Exception $e) {
            return response()->json(['success'=>false,'error'=>$mail->ErrorInfo],406);

        }

    }





    public function testEmergency(){
        $user=auth()->user()->id;
        $user_email=auth()->user()->email;
        $user_name=auth()->user()->name;
        $emergency_emails=EmailAddress::where('user_id',$user)->get();
        $phone_number=PhoneNumber::select('phone_number')->where('user_id',$user)->get();
        foreach ($emergency_emails as $emergency){
            //var_dump($emergency['email_address']);
            $this->mailObject($emergency['email_address'],$user_email);

        }
        //var_dump($phone_number);
        foreach ($phone_number as $item) {

            $mobile_number=$item["phone_number"];
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://api.msg91.com/api/sendhttp.php?sender=MSGIND&route=4&mobiles=".$mobile_number."&authkey=230460A32Zvsrd0awk5b698b9c&country=0&message=TEST! There's An Emergency Test Raised By ".$user_name."",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
        }

        Emergency::create([
            'user_id'=>$user
        ]);
        return response()->json(['success'=>true,'message'=>'Emergency Raised Successfully'],200);



    }
}
